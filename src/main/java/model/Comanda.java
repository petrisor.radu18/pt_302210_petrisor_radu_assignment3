package model;

import java.util.ArrayList;
import java.util.List;

import dao.ProductDao;
/**
 * 
 * @author radu_Se instantiaza tabelul Comanda cu coloanele numeclient,numeprodus,cantiate,total((numeclient,numeprodus) fiind primary key)
 *
 */

public class Comanda {
		private String numeclient;
		private String numeprodus;
		private int cantitate;
		private float total;
		 public Comanda() {}
		public Comanda(String cnume,String numeprodus,int c)
		{
			this.numeclient=cnume;
			this.numeprodus=numeprodus;
			ProductDao daoP=new ProductDao();
			List<Product> allp=new ArrayList<Product>();
			allp=daoP.getAllProducts();
			this.cantitate=c;
			for(Product p:allp)
			{
				if(p.isNume().equals(this.numeprodus))
				{
					this.total=p.getPret()*this.cantitate;
				}
			}
		}
		public float getTotal()
		{
			return this.total;
		}
		public void setTotal(float t)
		{
			this.total=t;
		}
		public String getNumeclient()
		{
			return this.numeclient;
		}
		public String getNumeprodus()
		{
			return this.numeprodus;
		}
		public int getCantitate()
		{
			return this.cantitate;
		}
		public void setCantitate(int c)
		{
			this.cantitate=c;
		}
		public void setNumeclient(String d)
		{
			this.numeclient=d;
		}
		public void setNumeprodus(String n)
		{
			this.numeprodus=n;
		}
		public String toString()
		{
			return "NumeClient:"+this.numeclient+" Nume Produs:"+this.numeprodus+" Cantitate:"+this.cantitate+" Total:"+this.total;
		}


}
