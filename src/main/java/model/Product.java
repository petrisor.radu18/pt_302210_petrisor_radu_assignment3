package model;
/**
 * 
 * @author radu_Se instantiaza tabelul Product cu coloanele nume,cantitate,pret (nume fiind primary key)
 *
 */

public class Product {
	private String nume;
	private int cantitate;
	private float pret;
	public Product() {}
   public Product(String n,int c,float p)
   {
	   this.nume=n;
	   this.cantitate=c;
	   this.pret=p;
   }
   public void setNume(String n)
   {
	   this.nume=n;
   }
   public void setCantitate(int c)
   {
	   this.cantitate=c;
   }
   public void setPret(float p)
   {
	   this.pret=p;
   }
   public String isNume()
   {
	   return this.nume;
   }
   public float getPret()
   {
	   return this.pret;
   }
   public int getCantitate()
   {
	   return this.cantitate;
   }
   public String toString()
   {
	   return "Nume:"+this.nume+" Cantitate:"+this.cantitate+" Pret:"+this.pret;
   }
}
