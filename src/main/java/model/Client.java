package model;
/**
 * 
 * @author radu_Se instantiaza tabelul Client cu coloanele nume,adresa (nume fiind primary key)
 *
 */
public class Client {
    private String nume;
    private String adresa;
    public Client() {}
    public Client(String n, String a)
    {
    	this.nume=n;
    	this.adresa=a;
    }
    public void setNume(String n)
    {
    	this.nume=n;
    }
    public void setAdresa(String a)
    {
    	this.adresa=a;
    }
    public String getNume()
    {
    	return this.nume;
    }
    public String getAdresa()
    {
    	return this.adresa;
    }
    public String toString()
    {
    	return "Nume:"+this.nume+" Adresa:"+this.adresa;
    }
}
