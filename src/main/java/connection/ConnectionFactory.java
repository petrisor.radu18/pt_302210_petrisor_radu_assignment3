package connection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * 
 * @author radu_In aceasta clasa se face conexiunea cu baza de date si se inchide totodata conexiunea
 *
 */

public class ConnectionFactory {
	private static final Logger LOGGER= Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER= "com.mysql.cj.jdbc.Driver";
	private static final String DBURL="jdbc:mysql://localhost:3306/assignment3?autoReconnect=true&useSSL=false";
	private static final String USER ="root";
	private static final String PASS="petrisor.radu18";
	
  private static ConnectionFactory singleInstance=new ConnectionFactory();
  
  private ConnectionFactory()
  {
	  try {
		  Class.forName(DRIVER);
	  }catch(ClassNotFoundException e)
	  {
		  e.printStackTrace();
	  }
  }
  private Connection createConnection()
  {
	  Connection connection=null;
	  try
	  {
		  connection=DriverManager.getConnection(DBURL,USER,PASS);
	  }catch(SQLException e)
	  {
		  LOGGER.log(Level.WARNING,"Eroare cand s-a incercat conectarea la baza de date");
		  e.printStackTrace();
	  }
	  return connection;
  }
  public static Connection getConnection()
  {
	  return singleInstance.createConnection();
  }
  public static void close(Connection connection)
  {
	  if(connection!=null)
	  {
		  try 
		  {
			  connection.close();
		  }catch(SQLException e)
		  {
			  LOGGER.log(Level.WARNING,"Eroare cand s-a incercat inchiderea statement");
			  e.printStackTrace();
		  }
	  }
  }
  public static void close(ResultSet resultSet)
  {
	  if(resultSet!=null)
	  {
		  try
		  {
			  resultSet.close();
		  }catch(SQLException e)
		  {
			  LOGGER.log(Level.WARNING,"Eroare cand s-a incercat inchiderea ResultSet");
		  }
	  }
  }
  public static void close(Statement p)
  {
	  if(p!=null)
	  {
		  try 
		  {
			  p.close();
		  }catch(SQLException e)
		  {
			  LOGGER.log(Level.WARNING,"Eroare cand s-a incercat inchiderea Statement");
		  }
	  }
  }
  
}
