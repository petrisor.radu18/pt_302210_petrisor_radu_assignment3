package start;
import presentation.Controller;
/**
 * 
 * @author radu_Aceasta clasa este clasa main, adica thread-ul principal
 *
 */
public class MainStart {

	public static void main(String[] args) {
		Controller MainControl = new Controller(args[0]);
		MainControl.verificareActiuni();
	}

}