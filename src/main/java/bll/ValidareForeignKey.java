package bll;

import java.util.ArrayList;
import java.util.List;
import dao.ClientDao;
import dao.ProductDao;
import model.Client;
import model.Comanda;
import model.Product;
/**
 * *?
 * @author radu_In aceasta clasa se verifica daca s-a indeplinit constrangerea de cheie straina pentru tabelul Comanda
 *
 */
	
public class ValidareForeignKey {
	Comanda o;
	ClientDao daoC;
	ProductDao daoP;
	public ValidareForeignKey(Comanda o)
	{
		this.o=o;
		this.daoC=new ClientDao();
		this.daoP=new ProductDao();
	}
	public int validare()
	{
		List<Client> listaClienti=new ArrayList<Client>();
		List<Product> listaProduse=new ArrayList<Product>();
		listaClienti=daoC.getAllClients();
		listaProduse=daoP.getAllProducts();
		Product p1=new Product();
		int []ok=new int[3];
		ok[0]=0;ok[1]=0;ok[2]=0;
		for(Client c:listaClienti)
		{
			if(o.getNumeclient().equals(c.getNume()))
			{
				ok[0]=1;
			}
		}
		for(Product p:listaProduse)
		{
			if(o.getNumeprodus().equals(p.isNume()))
			{
				ok[1]=1;
				if(o.getCantitate()<=p.getCantitate())
				{
					ok[2]=1;
					p1=p;
				}
			}
		}
		if(ok[0]==1 && ok[1]==1 && ok[2]==1)
		{
			daoP.updateProduct(p1,p1.getCantitate()-o.getCantitate());
			return 1;
		}
		return 0;
	}

}
