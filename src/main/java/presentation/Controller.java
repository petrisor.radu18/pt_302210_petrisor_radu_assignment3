package presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import dao.ClientDao;
import dao.OrderDao;
import dao.ProductDao;
import model.Client;
import model.Comanda;
import model.Product;
/**
 * 
 * @author radu_In aceasta clasa se extrag comenzile date in fisierul .txt si in functie de comanda, se vor efectua 
 * apeluri pentru afisare,inserare,stergere,updatare
 *
 */
public class Controller {
	  File fisierIntrare;
	  View v;
	  int nr=0;
	    public Controller(String fisier)
	    {
	    	this.v=new View();
	    	this.fisierIntrare=new File(fisier);
	    }
	    public void verificareActiuni()
	    {
	    	try {
				Scanner scanner=new Scanner(this.fisierIntrare);
				String delimiters="[:|,]";
				String delimiters1="[ ]";
				String[] tokens;
				String[] despartire;
				while(scanner.hasNext())
				{
					String propozitie=scanner.nextLine();
					System.out.println(propozitie);
					tokens=propozitie.split(delimiters);
					despartire=tokens[0].split(delimiters1);
					if(despartire[0].equalsIgnoreCase("Insert"))
					{this.insertToken(tokens, despartire[1]);}
					if(despartire[0].equalsIgnoreCase("Delete"))  /** In aceasta metoda se verifica ce tip de actiune este*/
					{this.deleteToken(tokens, despartire[1]);}   /** Dupa care se fac apelurile necesare*/
					if(despartire[0].equalsIgnoreCase("Order"))
					{this.orderToken(tokens);this.nr++;}
					if(despartire[0].equalsIgnoreCase("Report"))
					{this.reportToken(despartire[1]);this.nr++;}
				}
				scanner.close();
				System.out.println("Sfarsit");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	    }
	    private int deleteToken(String tokens[],String numeTabel)
	    {
	    	ClientDao daoC=new ClientDao();
    	    ProductDao daoP=new ProductDao();
    	    List<Product> delp=new ArrayList<Product>();
    	    List<Client> delc=new ArrayList<Client>();
    	if(numeTabel.equalsIgnoreCase("client"))
    	{
    		delc=daoC.getAllClients();
    		for(Client c:delc)
    		{
    			if(c.getNume().equals(tokens[1]))
    			{
    				daoC.deleteClient(c);
    			}
    		}
    		return 1;
    	}
    	if(numeTabel.equalsIgnoreCase("product"))
    	{
    		delp=daoP.getAllProducts();
    		for(Product p:delp)
    		{
    			if(p.isNume().equals(tokens[1]))
    			{
    				daoP.deleteProduct(p);
    			}
    		}
    		return 1;
    	}
    	return 0;
	    }
	    private int insertToken(String[] tokens,String numeTabel)
	    {
	    	ClientDao daoC=new ClientDao();
	    	ProductDao daoP=new ProductDao();
	    	if(numeTabel.equalsIgnoreCase("client"))
	    	{
	    		daoC.inserareClient(new Client(tokens[1],tokens[2]));
	    		return 1;
	    	}
	    	if(numeTabel.equalsIgnoreCase("product"))
	    	{
	    		String t1=tokens[2].trim();
	    		String t2=tokens[3].trim();
	    		int c=Integer.parseInt(t1);
	    		float f=Float.parseFloat(t2);
	    		daoP.inserareProduct(new Product(tokens[1],c,f));
	    		return 1;
	    	}
	    	return 0;
	    }
	    private int orderToken(String[] tokens)
	    {
	    	OrderDao daoO=new OrderDao();
	    	String t=tokens[3].trim();
	    	int c=Integer.parseInt(t);
	    	Comanda com=new Comanda(tokens[1],tokens[2],c);
	    	Comanda com1=new Comanda();
	    	daoO.inserareOrder(com);
	    	com1=daoO.getOrderBy("numeclient",com.getNumeclient(),"numeprodus",com.getNumeprodus());
	    	List<Comanda> co=new ArrayList<Comanda>();
	    	if(com1!=null)
	    	{
	    	co.add(com1);
	    	v.afisareOrders(co,nr);
	    	}
	    	else
	    	{
	    		v.afisareNull(nr);
	    	}
	    	return 1;
	    }
	    private void reportToken(String numeTabel)
	    {
	    	ClientDao daoC=new ClientDao();
    	    ProductDao daoP=new ProductDao();
    	    OrderDao daoO=new OrderDao();
    	    List<Product> allp=new ArrayList<Product>();
    	    List<Client> allc=new ArrayList<Client>();
    	    List<Comanda> allo=new ArrayList<Comanda>();
    	    if(numeTabel.equalsIgnoreCase("client"))
    	    {
    	    	allc=daoC.getAllClients();
    	    	v.afisareClienti(allc,nr);
    	    	
    	    }
    	    if(numeTabel.equalsIgnoreCase("product"))
    	    {
    	    	allp=daoP.getAllProducts();
    	    	v.afisareProducts(allp,nr);
    	    }
    	    if(numeTabel.equalsIgnoreCase("order"))
    	    {
    	    	allo=daoO.getAllOrders();
    	    	v.afisareOrders(allo,nr);
    	    }
    	    
	    }
}
