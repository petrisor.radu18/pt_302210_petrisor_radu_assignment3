package presentation;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Stream;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import model.Client;
import model.Comanda;
import model.Product;
/** 
 * 
 * @author radu_In aceasta clasa se creeaza fisierele pdf corespunzatoare fiecarei afisari, iar in acele fisiere se
 * insereaza tabele corespunzatoare actiunii
 *
 */
public class View {
	public View()
	{}
	public void afisareClienti(List<Client> lista,int nr)
	{
		Document d=new Document();
		try {
			String s=Integer.toString(nr);
			String s1="Report".concat(s);
			String s2=s1.concat(".pdf");
			PdfWriter.getInstance(d,new FileOutputStream(s2));
			d.open();
			PdfPTable tabel=new PdfPTable(2);
			addTabelHeaderClient(tabel);
			tabel.addCell("Nume Client");
			tabel.addCell("Adresa");
			for(Client c:lista)
			{
				addRowsClient(tabel,c);
			}
			d.add(tabel);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d.close();
	}
	private void addRowsClient(PdfPTable tabel, Client c)
	{
		tabel.addCell(c.getNume());
		tabel.addCell(c.getAdresa());
	}
	private void addTabelHeaderClient(PdfPTable tabel) {
		Stream.of("Client","").forEach(columnTitle->
		{
			PdfPCell header= new PdfPCell();
			header.setBorderWidth(0);
			header.setBackgroundColor(BaseColor.ORANGE);
			header.setPhrase(new Phrase(columnTitle));
			tabel.addCell(header);
		});
	}
	public void afisareProducts(List<Product> lista,int nr)
	{     Document d=new Document();
		try {
			String s=Integer.toString(nr);
			String s1="Report".concat(s);
			String s2=s1.concat(".pdf");
			PdfWriter.getInstance(d,new FileOutputStream(s2));
			PdfPTable tabel=new PdfPTable(3);
			d.open();
			addTabelHeaderProduct(tabel);
			tabel.addCell("Nume Produs");
			tabel.addCell("Cantitate");
			tabel.addCell("Pret");
			for(Product p:lista)
			{
				addRowsProduct(tabel,p);
			}
			d.add(tabel);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d.close();
	}
	private void addRowsProduct(PdfPTable tabel, Product p)
	{
		    String s1=String.valueOf(p.getCantitate());
			String s2=String.valueOf(p.getPret());
			tabel.addCell(p.isNume());
			tabel.addCell(s1);
			tabel.addCell(s2);	
	}
	private void addTabelHeaderProduct(PdfPTable tabel) 
	{
		Stream.of("","Produs","").forEach(columnTitle->
		{
			PdfPCell header= new PdfPCell();
			header.setBorderWidth(0);
			header.setBackgroundColor(BaseColor.ORANGE);
			header.setPhrase(new Phrase(columnTitle));
			tabel.addCell(header);
		});
	}
	public void afisareOrders(List<Comanda> lista,int nr)
	{
		Document d=new Document();
		try {
			String s=Integer.toString(nr);
			String s1="Report".concat(s);
			String s2=s1.concat(".pdf");
			PdfWriter.getInstance(d,new FileOutputStream(s2));
			d.open();
			PdfPTable tabel=new PdfPTable(4);
			addTabelHeaderOrder(tabel);
			tabel.addCell("Nume Client");
			tabel.addCell("Nume Produs");
			tabel.addCell("Cantitate");
			tabel.addCell("Pret total");
			for(Comanda com:lista)
			{
				addRowsOrder(tabel,com);
			}
			d.add(tabel);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d.close();
	}
	private void addRowsOrder(PdfPTable tabel, Comanda ord)
	{
		String s1=String.valueOf(ord.getCantitate());
		String s2=String.valueOf(ord.getTotal());
		tabel.addCell(ord.getNumeclient());
		tabel.addCell(ord.getNumeprodus());
		tabel.addCell(s1);
		tabel.addCell(s2);	
	}
	private void addTabelHeaderOrder(PdfPTable tabel) 
	{
		Stream.of("","Order","","").forEach(columnTitle->
		{
			PdfPCell header= new PdfPCell();
			header.setBorderWidth(0);
			header.setBackgroundColor(BaseColor.ORANGE);
			header.setPhrase(new Phrase(columnTitle));
			tabel.addCell(header);
		});
	}
	public void afisareNull(int nr)
	{
		Document d=new Document();
		try {
			String s=Integer.toString(nr);
			String s1="Report".concat(s);
			String s2=s1.concat(".pdf");
			PdfWriter.getInstance(d,new FileOutputStream(s2));
			d.open();
			Chunk propozitie=new Chunk("Nu se poate indeplini aceasta comanda");
			d.add(propozitie);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d.close();
	}

}
