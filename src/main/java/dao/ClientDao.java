package dao;
import java.util.ArrayList;
import java.util.List;
import model.Client;
public class ClientDao extends AbstractDao<Client>{
	public ClientDao()
	{
		super();
	}
	/**
	 * 
	 * @param c clientul care trebuie inserat in tabela
	 */
	public void inserareClient(Client c)
	{
		super.insert(c);
	}
	/**
	 * 
	 * @param c clientul care trebuie sters din tabela
	 */
	public void deleteClient(Client c)
	{
		if(super.delete("nume",c.getNume(),null,null)==1)
		{}
	}
	/**
	 * 
	 * @return returneaza toti clientii care se afla in tabela
	 */
	public List<Client> getAllClients()
	{
		List<Client> lista=new ArrayList<Client>();
		lista=super.findAll();
		return lista;
	}
	/**
	 * 
	 * @param camp Numele coloanei
	 * @param val  Valoarea din acea coloana
	 * @return clientul care s-a gasit
	 */
	public Client getClientBy(String camp,String val)
	{
		Client c=super.findBy(camp, val,null,null);
		return c;
	}
}
