package dao;
import java.util.ArrayList;
import java.util.List;
import model.Product;
/**
* 
* @author radu_La fel ca si la clasa ClientDao
*
*/
public class ProductDao extends AbstractDao<Product>{
	public ProductDao()
	{
		super();
	}
	public void updateProduct(Product p,int canti)
	{
		String cant=String.valueOf(canti);
		super.update("nume","cantitate",p.isNume(),cant);
	}
	public void inserareProduct(Product p)
	{
		List<Product> lista=new ArrayList<Product>();
		lista=this.getAllProducts();
		int ok=1;
		int cant=0;
		for(Product p1:lista)
		{
			if(p1.isNume().equals(p.isNume()))
			{
				ok=0;
				cant=p.getCantitate();
			}
		}
		if(ok==1)
		{
		super.insert(p);
		}
		else
		{
			this.updateProduct(p,p.getCantitate()+cant);
		}
	}
	public void deleteProduct(Product p)
	{
		if(super.delete("nume",p.isNume(),null,null)==1)
		{}
	}
	public List<Product> getAllProducts()
	{
		List<Product> lista=new ArrayList<Product>();
		lista=super.findAll();
		return lista;
	}
	public Product getProductBy(String camp,String val)
	{
		Product p=super.findBy(camp, val,null,null);
		return p;
	}

}
