package dao;
import java.util.ArrayList;
import java.util.List;
import bll.ValidareForeignKey;
import model.Comanda;
/**
 * 
 * @author radu_La fel ca si la clasa ClientDao
 *
 */
public class OrderDao extends AbstractDao<Comanda>{
	public OrderDao()
	{
		super();
	}
	public void inserareOrder(Comanda o)
	{
		ValidareForeignKey validare=new ValidareForeignKey(o);
		
		if(validare.validare()==1)
		{super.insert(o);}
		else 
		{
			System.out.println("Nu exista clientul sau produsul in baza de date sau cantitea ceruta este mult prea mare");
		}
	}
	public void deleteOrder(Comanda o)
	{
		if(super.delete("numeclient",o.getNumeclient(),"numeprodus",o.getNumeprodus())==1)
		{}
	}
	public List<Comanda> getAllOrders()
	{
		List<Comanda> lista=new ArrayList<Comanda>();
		lista=super.findAll();
		return lista;
	}
	public Comanda getOrderBy(String camp,String val,String camp1,String val1)
	{
		Comanda o=super.findBy(camp, val,camp1,val1);
		return o;
	}

}
