package dao;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import connection.ConnectionFactory;
import model.Client;
import model.Comanda;
import model.Product;
public class AbstractDao<T> {
     protected static  final Logger LOGGER=Logger.getLogger(AbstractDao.class.getName());
     private final Class<T> tip;
     @SuppressWarnings("unchecked")
     public AbstractDao()
     {
    	 this.tip=(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
     }
     private String creeazaQueryInsert()                 /** Se creeaza query-ul pentru inserare */
     {
    	 StringBuilder interogare=new StringBuilder();
    	 interogare.append("INSERT INTO ");
    	 interogare.append(tip.getSimpleName());                           
    	 interogare.append("(");
    	 int nrCampuri=0;
    	 int ok=0;
    	 for(Field campuri:tip.getDeclaredFields())
    	 {nrCampuri++;
    	 if(ok==0)
    	 {
    		 interogare.append(campuri.getName());
    		 ok=1;
    	 }
    	 else {
    	interogare.append(","+campuri.getName());
    	 }
    	 }
    	 interogare.append(")\n"); 
    	 interogare.append(" VALUES(?");
    	 for(int i=1;i<nrCampuri;i++)
    	 { interogare.append(",?");}
    	 interogare.append(")");
    	 return interogare.toString();
     }
     private String creeazaQueryDelete(String camp,String camp1)    /** Se creeaza query-ul pentru stergere, daca se sterge o comanda vom avea 2  campuri de verificat*/
     {
    	 StringBuilder interogare=new StringBuilder();
    	 interogare.append("DELETE ");
    	 interogare.append(" FROM ");
    	 interogare.append(tip.getSimpleName());
    	 if(camp1!=null)
    	 {interogare.append(" WHERE "+camp+" =?"+" AND "+camp1+" =?");}
    	 else
    	 { interogare.append(" WHERE "+camp+" =?"); }
    	 return interogare.toString();
     }
     private String creeazaQueryUpdate(String camp,String coloana,String valoare)  /** Se creeaza query-ul pentru update */
     {
    	 StringBuilder interogare=new StringBuilder();
     	interogare.append("UPDATE ");
     	interogare.append(tip.getSimpleName());
     	interogare.append(" SET ");
     	interogare.append(coloana);
     	interogare.append(" = ");
     	interogare.append(valoare);
     	interogare.append(" WHERE " +camp+" =?");
     	return interogare.toString(); 
     }
     private String creeazaQuery(String camp,String camp1)              /** Se creeaza query-ul pentru cautarea unui obiect*/
     {
    	 StringBuilder interogare=new StringBuilder();
    	 interogare.append("SELECT ");
    	 interogare.append(" * ");
    	 interogare.append(" FROM ");
    	 interogare.append(tip.getSimpleName());
    	 interogare.append(" WHERE "+camp+" =?"+" AND "+camp1+" =?");
    	 return interogare.toString();
     }
     private String creeazaQueryAll()                                    /** Se creeaza query-ul pentru cautarea tuturor elementelor din tabela*/
     {
    	 StringBuilder interogare=new StringBuilder();
    	 interogare.append("SELECT ");
    	 interogare.append(" * ");
    	 interogare.append(" FROM ");
    	 interogare.append(tip.getSimpleName());
    	 return interogare.toString();
     }
     public List<T> findAll()
     {
    	 Connection connection=null;
    	 PreparedStatement statement=null;
    	 ResultSet tuple=null;
    	 String query= creeazaQueryAll();
    	 try 
    	 {
    		 connection=ConnectionFactory.getConnection();
    		 statement=connection.prepareStatement(query);
    		 tuple=statement.executeQuery();
    		return createObjects(tuple);
    	 }catch(SQLException e)
    	 {
    		 LOGGER.log(Level.WARNING,tip.getName()+ "DAO:findALL"+e.getMessage());
    	 }finally 
    	 {
    		 ConnectionFactory.close(tuple);
    		 ConnectionFactory.close(statement);
    		 ConnectionFactory.close(connection);
    	 } 
    	 return null;
     }
     public T findBy(String camp,String val,String camp1, String val1) /** Se face cautarea unui element dintr-o tabela */
     {
    	 Connection connection=null;
    	 PreparedStatement statement=null;
    	 ResultSet tuple=null;
    	 String query= creeazaQuery(camp,camp1);
    	 try 
    	 {
    		 connection=ConnectionFactory.getConnection();
    		 statement=connection.prepareStatement(query);
    		 statement.setString(1,val);
    		 statement.setString(2,val1);
    		 tuple=statement.executeQuery();
    		 int ok=0;
    		 if(tuple.next()==false)
    		 {ok=1; }                                                   /** Daca nu exista acel element se va returna null si se va returna un mesaj special in pdf */
    		 else 
    		 {tuple.beforeFirst();}
    		 if(ok==0)
    		 return createObjects(tuple).get(0);
    		 else return null;
    	 }catch(SQLException e)
    	 {
    		 LOGGER.log(Level.WARNING,tip.getName()+ "DAO:findBy"+e.getMessage());
    	 }finally 
    	 {
    		 ConnectionFactory.close(tuple);
    		 ConnectionFactory.close(statement);
    		 ConnectionFactory.close(connection);
    	 } 
    	 return null;
     }
     public int insert(Object o)                               /** Se face inserarea unui element intr-o tabela */
     {
    	 Connection connection=null;
    	 PreparedStatement statement=null;
    	 ResultSet tuple=null;
    	 String query=creeazaQueryInsert();
    	 try 
    	 {
    	 connection=ConnectionFactory.getConnection();
    	 statement=connection.prepareStatement(query);
    	 if(o instanceof Client)
    	 {
    		 Client c=(Client)o;
    		 statement.setString(1,c.getNume());
    		 statement.setString(2,c.getAdresa());
    	 }
    	 else if(o instanceof Product)
    	 {
    		 Product p=(Product)o;
    		 statement.setString(1,p.isNume());
    		 statement.setInt(2,p.getCantitate());
    		 statement.setFloat(3,p.getPret());
    	 }
    	 else if(o instanceof Comanda)
    	 {
    		 Comanda or=(Comanda)o;
    		 statement.setString(1,or.getNumeclient());
    		 statement.setString(2,or.getNumeprodus());
    		 statement.setInt(3, or.getCantitate());
    		 statement.setFloat(4,or.getTotal());
    	 }
    	 statement.executeUpdate();
    	 return 1;
    	 }catch(SQLException e)
    	 {
    		 LOGGER.log(Level.WARNING,tip.getSimpleName() + " DAO:insert "+e.getMessage());
    	 }finally 
    	 {
    		 ConnectionFactory.close(tuple);
    		 ConnectionFactory.close(statement);
    		 ConnectionFactory.close(connection);
    	 } 
    	 return 0;
     } 
     public int delete(String camp,String valoare,String camp1,String valoare1)    /** Se face stergerea unui element dintr-o tabela, daca este o comanda este necesara verificarea a 2 campuri*/
     {
    	 Connection connection=null; PreparedStatement statement=null;ResultSet tuple=null;String query=null;
    	 if(tip.getSimpleName().equals("Comanda"))
    	 { query=creeazaQueryDelete(camp,camp1); }
    	 else
    	 { query=creeazaQueryDelete(camp,null); }
    	 try 
    	 {
    	 connection=ConnectionFactory.getConnection();
    	 statement=connection.prepareStatement(query);
    	 if(tip.getSimpleName().equals("Comanda"))
    	 {
    		 statement.setString(1,valoare);
    		 statement.setString(2, valoare1);
    	 }
    	 else
    	 {statement.setString(1,valoare); }
    	 statement.executeUpdate();
    	 return 1;
    	 }catch(SQLException e)
    	 {
    		 LOGGER.log(Level.WARNING,tip.getName()+ "DAO:delete"+e.getMessage());
    	 }finally 
    	 {
    		 ConnectionFactory.close(tuple);
    		 ConnectionFactory.close(statement);
    		 ConnectionFactory.close(connection);
    	 } return 0;}
     public int update(String camp,String coloana,String valoare,String cantitateNoua) /** Se face updatarea unui element dintr-o tabela */
     {
    	 Connection connection=null;
    	 PreparedStatement statement=null;
    	 ResultSet tuple=null;
    	 String query=creeazaQueryUpdate(camp,coloana,cantitateNoua);
    	 try 
    	 {
    	 connection=ConnectionFactory.getConnection();
    	 statement=connection.prepareStatement(query);
    	 statement.setString(1,valoare);
    	 statement.executeUpdate();
    	 return 1;
    	 }catch(SQLException e)
    	 {
    		 LOGGER.log(Level.WARNING,tip.getName()+ "DAO:update"+e.getMessage());
    	 }finally 
    	 {
    		 ConnectionFactory.close(tuple);
    		 ConnectionFactory.close(statement);
    		 ConnectionFactory.close(connection);
    	 } 
    	 return 0;} 
     private List<T> createObjects(ResultSet tuple)
     {List<T> list=new ArrayList<T>();
    	 try {
    		 while(tuple.next()){                                                /** Se creeaza mai multe obiect in functie de cate randuri au fost returnate dupa executarea queriului */
    			 T instance;
    			 instance=tip.newInstance();
    			 for(Field camp:tip.getDeclaredFields())
    			 {   Object value=tuple.getObject(camp.getName());
    				 PropertyDescriptor pd=new PropertyDescriptor(camp.getName(),tip);
    				 Method metoda=pd.getWriteMethod();
    				 metoda.invoke(instance, value); 
    				 }list.add(instance);  }
    	 } catch (InstantiationException e) {
 			e.printStackTrace();
 		} catch (IllegalAccessException e) {
 			e.printStackTrace();
 		} catch (SecurityException e) {
 			e.printStackTrace();
 		} catch (IllegalArgumentException e) {
 			e.printStackTrace();
 		} catch (InvocationTargetException e) {
 			e.printStackTrace();
 		} catch (SQLException e) {
 			e.printStackTrace();
 		} catch (IntrospectionException e) {
 			e.printStackTrace();
 		}
    	 return list;}}
